tags: chapter3 chapter3.2 sobolev_spaces

ID:rDfDZd1c1ZAIoC9oLiveIBDOVq#DLK
\def<<<
Sobolev norms
#
for [ $k\in \mathbb{N}_0 $ and $ 1\leq p < \infty $ ], let [ $ u \in L^1_{loc}(\Omega) $ have all weak derivatives for $ |\alpha| \leq k $ ]. We define the Sobolev norms:

[ $ \|u\|_{W^{k,p}(\Omega)} \left( \sum_{|\alpha| \leq k} \|D^\alpha u\|^p_{L^p(\Omega)}\right)^{\frac{1}{p}} $ ]
#
The idea is to sum op all the derivatives in an $L^p$-norm style.
The limit case $p=\infty$ is defined just like the $L^\infty$ norm:
$ \|u\|_{W^{k,\infty}} (\Omega) := \max_{|\alpha| \leq k} \|D^\alpha u \|_{L^\infty ( \Omega)} $
>>>

ID:h*~iiO@Kyr_xCoOXih2PbTrHo8nHQr
\def<<<
Sobolev space
#
for [$k\in \mathbb{N}_0 $ and $ 1\leq p < \infty $] we define the sobolev spaces:

[ $ W^{k,p}(\Omega) := \{u \in L^1_{loc} (\Omega) | \|u\|_{W^{k,\infty} < \infty } $ ]
#
You can do k (weak) derivatives of a function in $ W^{k,p} $.
For any k we have the inclusion: $ W^{k,p} \subset L^p $, because the condition says the $L^p$-norm is well-defined and bounded.
>>>

ID:Ed18WttMKXSS3FFnmrMJ*uWuYQgq_z
\card<<<
An easy way to write $ W^{0,p}(\mathbb{R}) $?
#
$ W^{0,p}(\mathbb{R})  = L^p(\mathbb{R}) $
>>>

ID:XV_PU?5SU24hNfTUIrGEof6r=^?6Gk
\theorem<<<
Sobolev spaces are Banach spaces
#
for [$k\in \mathbb{N}_0 $ and $ 1\leq p < \infty $],
[ $ W^{k,p}(\Omega) := \{u \in L^1_{loc} (\Omega) | \|u\|_{W^{k,\infty}} < \infty \} $ ] is a [Banach space]
#
??? prove completeness (because that is the critical part here, we see that it is a normed vector space already).
- We need to prove all Cauchy sequences converge. 
(Cauchy sequences use the norm like this: $ \forall \varepsilon>0 \quad \exists N\in\mathbb{N} \quad \forall m,n \ge N \colon \quad \left|a_m-a_n \right|<\varepsilon $ )
- We know $ L^p $ to be complete, and those norms are what the Sobolev norm is build of.
+ Because $ W^{k,p} \subset L^p $ we know that a Cauchy sequence $f_n$ in $ W^{k,p} $ converges to an $ f\in L^p $. We need to show that $ f\in W^{k,p} $ 
+ For $ |\alpha| \leq k $ we also have that for a Cauchy sequence $f_n$ the sequence $ D^\alpha f_n $ is Cauchy (in $L^p$) and therefore converges to some $ g \in L^p $
+ If we prove that $g=D^\alpha f$ we have that for any Cauchy sequence $f_n \in W^{k,p}$ the norm $ \|f_n - f\|_{W^{k,p}} $ converges to zero, and then we would have shown that $f_n \rightarrow f$
- To prove $ g=D^\alpha $ we first show that $\forall \phi \in \mathcal{D} \int_\Omega g \phi dx = lim_{n\rightarrow\infty} \int_\Omega D^\alpha f_n \phi dx  $.
+ with K the support of $\phi$ (which is compact):
$ \int_\Omega (D^\alpha f_n - g) \phi dx \leq \|D^\alpha f_n - g\|_{L^1(K)} \cdot \|\phi\|_{L^\infty(K)} 
\leq C \|D^\alpha f_n - f\|_{L^p(K)} \|\phi\|_{L^\infty(K)} \rightarrow 0 $
where the last inequality between $ L^1 $ and $L^p$ can be shown with boundedness of K and Cauchy-Schwarz-Inequality on $L^p$
- The definition of $ g=D^\alpha f $ is $ \int g \phi dx = (-1)^{|\alpha|} \int f D^\alpha \phi dx \quad \forall \phi\in\mathcal{D} $ and we can directly show that with the previous step and partial integration (which works well because $\phi$ vanisches at the boundary).
+  $ \int g \phi dx = \lim_{n\rightarrow\infty} \int D^\alpha f_n \phi dx = \lim (-1)^{|\alpha|} \int f_n D^\alpha \phi dx  = (-1)^{|\alpha|} \int f D^\alpha \phi dx \quad \forall \phi\in\mathcal{D} $ 
>>>

ID:_Zw&E-NBN?ba%o#-ALk-+GD9+zIxiH
\card<<<
What is $H^k$ and what is special about it?
#
$ H^k = W^{k,2} $. The Sobolev spaces with p=2 are Hilbert spaces, as their norm comes from a sum of $L_2$-inner products.
>>>

ID:fZXhV3TneZiFhOSfwJ~PK_Z&UOT+t?
\theorem<<<
denseness-description of Sobolev-Spaces
#
[$ C^\infty(\Omega) \cap W^{k,p}(\Omega) $] is [dense] in [$W^{k,p}$]
#
-no proof in lecture-
>>>

ID:oUT7&NP^jguz%aY0c^5CABWvjKh95K
\card<<<
For X and Y two Banach spaces, what does it mean to say "The compact embedding $ X \hookrightarrow Y $ holds"?
#
We have $ X \subset Y $ and the identity operator $ id: X \rightarrow Y; x \mapsto x $ is compact.
(Operator T compact means for every bounded series $x_n \subset X$ the series $ T x_n $ has a convergent subsequence in Y)
>>>

ID:cPeKfMr5^K%t-MBUUcY6PgxSPD#oW&
\theorem<<<
Sobolev-Compact-Embedding-Theorem
#
Let [$ \Omega \subset \mathbb{R} $] be an open, bounded domain with Lipschitz boundary and [$k_1\geq k_2,k_1,k_2\in \mathbb{N}_0 $] [$ p_1,p_2\in \lbrack 1,\infty) $]. If $\frac{k_1 - k_2}{d} >  \frac{1}{p_1} - \frac{1}{p_2} $, there holds [the compact embedding:
$ W^{k_1}{p_1}(\Omega) \hookrightarrow W^{k_2}{p_2} $]
#
examples:
$ H^2(\Omega) \hookrightarrow H^1(\Omega) $
$ for d=1,2,3: H^1(\Omega) \hookrightarrow ...$
---
no proof, deeper result
>>>
