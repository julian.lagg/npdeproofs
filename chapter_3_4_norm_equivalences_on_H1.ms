tags chapter3 chapter3.4


theorem<<<
Bramble-Hilbert-Lemma
#
Let $\Omega$ be [bounded], U be [a Hilbert space] and [$ L: H^k(\Omega) \rightarrow U $] be [a continuous linear operator] such that [$Lq=0$ for all polynomials $ q\in \mathcal{P}^{k-1} $]. Then there holds:
[$ \|Lv\|_U \lesssim \sum_{|\alpha|=k} \|D^\alpha v\|_{L^2(\Omega)} \forall v\in H^k(\Omega) $]
#
??? prove this.
- use the Tartar-theorem. Wich of the three parts jof the theorem is applicable and helpful in this setting?
- part 1, because w have a function with finite-dimensional kernel.
+ todo
>>>
