tags: chapter2 chapter2.4

ID:zt~~Q%Zv*ylFn5R!todO&TO=AJL1Zk
\untitleddef<<<
A duality pairing between a [functional $ f \in V^* $] and [an object $ v \in V $]
is [$ f $ applied to $ v $]. We can denote this as
[$ f(v)$ or equivalently $ \langle f,v\rangle_{V^* \times V} $]
#
When the spaces are clear we neglect the index: $ \langle f,v \rangle $
>>>

ID:bsSrMiuCrFp6^wT%4*v0E@?5D9xcAr
\theorem<<<
operator-from-bilinear-form-lemma
#
A [continuous] bilinear form $ A(\cdot, \cdot) : V \times V \rightarrow \mathbb{R}$
induces a [continuous linear operator] $ A: V \rightarrow V^* $ via:

[$ \langle Au,v \rangle_{V^* \times V} = A(u,v) \forall u,v \in V $]

The operator norm $ \|A\|_{V\rightarrow V^*} $ is [bounded by
the continuity bound $ \alpha_2 $ of $ A(\cdot, \cdot) $ ].

#
??? prove the operator norm of the resulting function $ Au $ is finite, that is,
the operator A produces continuous functions (necessary for $Au \in V^*$ )
- use the operator norm definition and the one thing we know about $ A(u,v) $
+ $ \|A(u,\cdot)\|_{V^*} = \sup_{0\neq v\in V} \leq \sup \frac{\alpha_2 \|u\|_V \|v\|_V}{\|v\|_V} = \alpha_2 \|u\|_V $
??? prove the operator norm of A is bounded by $ \alpha_2 $
- operator norm definition twice
- what do we know about A?
- A is continuous, and that means $ A(u,v) \leq \alpha_2 \|u\|\|v\| $
+ $ \|A\|_{V\rightarrow V^*} = \sup_{0\neq u \in V} \frac{\|Au\|_{V^*}}{\|u\|_V} = \sup_u \sup_v \frac{\langle Au, v \rangle }{\|u\|\|v\|} = \sup \sup \frac{A(u,v)}{\|u\|\|v\|} \leq \alpha_2$
>>>

ID:&z88XX721c%&7!R@^w=?a2&boym9pv
\theorem<<<
Banach fixpoint theorem
#
given a [Banach] space $ V $ and $ T: V \rightarrow V $ with $ [\|T(v_1) - T(v_2)\| \leq L \|v_1 - v_2\| \forall v_1,v_2 \in V $
for an $ L \in \lbrack 0,1)]$. Then there exists [a unique fixed point $ u\in V $ that solves $ u=T(u) $.]

Further, the iteration given by [$ u^{k+1} := T(u^k) $] converges to u with [convergence rate L].
That means [$ \|u-u^{k+1}\| \leq L \|u-u^k\| $]
#
we use this to prove Lax Milgram Lemma
>>>

ID:VFdOqX@hNDNBSDUVfrhv04FwVI5A2I
\def<<<
Riesz isomorphism $J_V$
#
$ J_V:$ [$ V^* \rightarrow V $] with:  [$ \forall u\in V \forall g\in V^* : (J_V g, u)_V = g(u) $.]
#
The Riesz isomorphism maps a functional to its Riesz Representer
>>>

ID:J7~MBL12X!yJp2E%y!IhWSC?oF@ks-
\theorem<<<
Lax-Milgram-Lemma
#
Given a [Hilbert] space $V$, a [coercive ($ \alpha_1 $) and continuous ($ \alpha_2 $)] bilinear form $A(\cdot,\cdot)$
and a continuous linear form f(\cdot). Then there exists a unique solution $ u\in V $
solving:

[$ A(u,v) = f(v) \forall v \in V $]

The solution fulfills the stability estimate:

[$\|u\|_V \leq \alpha_1^{-1} \|f\|_V $]
#
??? prove existence and uniqueness of the solution $ u\in V $
- We want to use Banach Fixpoint theorem
- What can we use to rewrite the problem as a fixpoint equation?
- We can get rid of the $ v $ in the equation.
- apply the Riesz isomorphism to remove the $ v $ while retaining that the equation is in $ V $, not $V^*$
+ $ A(u,v) = f(v) \forall v \in V $
$\Leftrightarrow J_V Au = J_V f$
$\Leftrightarrow J_v(Au - f) = 0 $
- now introduce a parameter to obtain an operator that has the solution $ u $ as a fixpoint
+ $ T(u) := u - \tau J_V (Au - f) $
- now we need to prove we can choose $ \tau $ st this is a contraction in the Banach sense
- start with $\|T(v_1) - T(v_2) \|$.
- actually, $\|T(v_1) - T(v_2) \|^2$ is the more practical formulation, so we get scalar products with no roots
+ Let $v_1,v_2 \in V, v=v_1 - v_2 : $
$\|T(v_1) - T(v_2)\|^2_V = \|T(v)\|_V^2 =  \|v - \tau J_V Av\|^2$
$ = \|v\|^2_V - 2 \tau (v, J_V Av) + \tau^2 \|J_V Av\|^2_V$
- we need to find an upper bound for this that is $ c \cdot \|v\|_V^2 $
- this is where we use coercivity and continuity of A
+ $(v,J_V Av) = \langle Av, v \rangle = A(v,v) \geq \alpha_1 \|v\|^2_V $
$\|J_V Av\|^2_V = \|Av\|^2_{V^*} \leq \alpha_2^2 \|v\|_V^2 $
$\Rightarrow \|T(v_1) - T(v_2)\|^2_V \leq (1-2\alpha_1\tau + \tau^2 \alpha_2^2) \cdot \|v\|^2_V $
- we need to choose $ \tau $ st $ T $ is a contraction.
- $\alpha_2 \geq \alpha_1 > 0 $
+ $ \tau=\frac{\alpha_1}{\alpha_2^2} $
+ $ L = 1- \left( \frac{\alpha_1}{\alpha_2} \right)^2 \in \lbrack 0,1)$
+ with Banach FPT: -> T has a unique fixpoint. Why does this show existence and uniqueness?
+ we have constructed T so that any fixpoint of T solves the variational equation.
Now we have shown that T has a unique fixpoint, therefore the variational equation has a unique solution.
??? prove the stability estimate $ \|u\|_V \leq \alpha_1^{-1} \|f\|_V $(assume existence and uniqueness is already proven)
- We know that A is coercive with constant $ \alpha_1 $
- We also know something about $ A(u,u) $, given that $ u $ solves the variational problem.
- We then use a very general way to get an upper bound for $ f(u) $
+ $ \|u\|^2_V \leq \alpha_1^{-1} A(u,u) = \alpha_1^{-1} f(u) \leq \alpha_1^{-1} \|f\| \|u\|$
+ divide by $ \|u\| $ to get the result
>>>