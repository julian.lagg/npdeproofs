tags: chapter2 chapter2.6 infsup_stability

ID:7d~hH^_zI!PF@*5oWHWt?ZTryDnx+F
\def<<<
inf-sup-stability
#
Let $V$ and $W$ be [Hilbert spaces] and $B:V\times W \rightarrow \mathbb{R} $ be [a continuous ($\beta_2$) bilinear] form.
The inf-sup-condition is:
[$ \inf\limits_{0\neq v \in V} \sup\limits_{0\neq w \in W} \frac{B(v,w)}{\|v\|_V \|w\|_W} \geq \beta_1 > 0 $]
#
equivalent formulations:
$ \forall u \in V \sup\limits_{0\neq v\in W} \frac{B(u,v)}{\|v\|_W} \geq \beta_1 \|u\|_V $

$ \forall u \in V, \exists 0 \neq v \in W, s.t. B(u,v) \geq \beta_1 \|u\|_V \|v\|_W  $

written with operator $ B:V\rightarrow W^*$
$ \|Bu\|_{W^*} = \sup\limits_{0\neq v \in W} \frac{Bu(v)}{\|v\|_W} = \sup\limits_{0\neq v \in W} \frac{B(u,v)}{\|v\|_W} \geq \|u\|_V $
>>>

ID:?n?mFxch=ja4#L6xRLxfy%buU*L#^*
\card<<<
formulate the inf-sup-condition for $ B: V \times W \rightarrow \mathbb{R} $ in terms of the operator $ B: V \rightarrow W^* $ 
#
$ \|Bu\|_{W^*} = \sup\limits_{0\neq v \in W} \frac{Bu(v)}{\|v\|_W} = \sup\limits_{0\neq v \in W} \frac{B(u,v)}{\|v\|_W} \geq \|u\|_V $
>>>

ID:sp&mItw@5fBbT^NGPT1H#M=Kj&O%ux
\theorem<<<
injectivity-characterization of linear maps
#
A linear map is [injective] if and only if its kernel [is {0}]
#
??? prove injective => trivial kernel
+ By linearity: $ f(0) = 0$, by injectivity: $f(x)=0 \Rightarrow x=0 $
??? prove trivial kernel => injective
- What can we say about $ x-y $ if $ f(x) = f(y) $ ?
- $x-y$ can be shown to be in the kernel of f. Why is this helpful?
- because of the trivial kernel, $x-y \in ker f \Rightarrow x=y $
+ $f(x) = f(y) \Leftrightarrow f(y - x) = 0 \Leftrightarrow y - x \in Ker f \Rightarrow x=y $
>>>

ID:=FHXF4oDqdn&D?#bW~^Oa_F=IfCA%O
\theorem<<<
infsupstability implies uniqueness
#
if $B : V \times W \rightarrow \mathbb{R} $ is [inf-sup-stable] than a solution to $B(u,v) = f(v)$ is unique if [it exists].
#
??? prove this
- we want to prove that $ B : V \rightarrow W^* $ is injective, because then the solution to $Bu=f$ is unique in $W^*$
- use the inf-sup-criterion written in terms of the operator B.
- infsupcriterion is $ \|Bu\|_{W^*} \geq \beta_1 \|u\|_V $
- injectivity $\Leftrightarrow$ trivial kernel
+ assume B has a nontrivial kernel, choose $x \neq y, x,y\in ker B $
$\|B(x-y)\|_{W^*} = ? $
+ $ \|B(x-y)\|_{W^*} = \|Bx - By\| = 0 $
- why is this a contradiction?
+ from inf-sup-stability we have $ 0 = \|B(x-y)\|_{W^*} \geq \beta_1 \|x-y\| > 0 $
>>>

