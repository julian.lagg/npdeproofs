tags: chapter2 chapter2.5 galerkin_method

ID:J39M*+Qh~E6r5CnJQD7I^FcdAmhq3S
\def<<<
Galerkin approximation/method
#
Let $ A(u,v) = f(v) \,\, \forall v\in V \, $ be the variational problem.
Let $V_h$ be [a closed subspace of $V$].
The approximation of the problem is:
[$ A(u_h,v_h) = f(v_h) \, \forall v_h \in V_h$]
#
$V_h$ is typically finite-dimensional.
>>>

ID:7#X+*gqPCiCne8V9?kiR=LZOtWq6s6
\theorem<<<
Galerkin orthogonality property
#
Consider the problem [$ A(u,v) = f(v)\,\forall v \in V$]
and its Galerkin approximation [$ A(u_h,v_h) = f(v_h) \forall v_h \in V_h$].
Then we have:
[$ A(u-u_h , v_h) = 0 \, \forall v_h \in V_h $]
#
??? prove this orthogonality
- bilinearity
- both $u$ and $u_h$ solve the variational problem in $ V_h $
+ $ A(u-u_h,v_h) = A(u,v_h) - A(u_h, v_h) = f(v_h) - f(v_h) = 0 $
>>>

ID:sa?7jUrBRuZfBO7~?sNWoriTZL^RrC
\theorem<<<
inheritance of coercivity and continuity
#
Given a coercive, continuous bilinear form $ A : V \times V \Rightarrow \mathbb{R} $
with a Hilbert space $V$ and $V_h$ a closed subspace of $V$.
Then A is also [a coercive and bilinear form on $ V_h\times V_h $].
#
This just follows from the fact that $V_h \subset V $ and the inequalities for
continuity and coercivity hold for all vectors in V.
This is important because this gives us that the discrete problem is uniquely solvable
by the Lax-Milgram-Lemma.
>>>


ID:j_AJIHse-bi*1Rjm10A2VPex9C%v3%
\theorem<<<
Cea-Theorem
#
Let $ u $ be the solution to $ A(u,v) = f(v) \,\forall v\in V $,
and $u_h $ the solution to the Galerkin approximation of the same equation.

$A$ is coercive ($\alpha_1$) and continuous ($\alpha_2$), $f$ is [continuous].

The discretization error, defined as [$ \|u - u_h\|_V $], is [quasioptimal].
This means:
[$ \|u-u_h\|_V \leq \frac{\alpha_2}{\alpha_1} \inf_{v_h \in V_h} \|u-v_h\|_V $ ]
(meaning that $u_h$ is "almost the best" possible approximation of $u$ in $V_h$,
but the quality of the approximation is limited by properties of A)
#
??? prove this.
- it is easier to start with $\|u-u_h\|^2 $ and prove
$ \|u-u_h\|^2 \leq \frac{\alpha_2}{\alpha_1}\|u-u_h\| \|u-v_h\| \,\forall v_h \in V_h$,
then divide by $ \|u-u_h\|_V $
- The first step is to use coercivity of A.
+ $\|u-u_h\|^2 \leq \alpha_1^{-1} A(u-u_h, u-u_h) $
- we need to put an arbitrary $v_h \in V_h $ into the equation. How can we do this?
- by adding a zero.
- The Galerkin orthogonality property gives us that $ 0 = A(u-u_h,x) \,\forall x \in V_h $
- choosing $x$ to be $v_h - u_h$, we get something that nicely combines with the A we already have.
- we want to get an A that has our $u-v_h$ as the second argument to get our result by continuity of A
+ $\|u-u_h\|^2 \leq \alpha_1^{-1} A(u-u_h, u-u_h) - \alpha_1^{-1} A(u-u_h,v_h - u_h) $
$ = \alpha_1^{-1} A(u-u_h, u-v_h) $
- continuity of A
+ $ \leq \frac{\alpha_2}{\alpha_1}\|u-u_h\| \|u-v_h\| $
+ $ \Rightarrow \|u-u_h\| \leq \frac{\alpha_2}{\alpha_1} \|u-v_h\| $
>>>