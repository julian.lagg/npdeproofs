tags: chapter3 chapter3.1 generalized_derivatives

ID:UciprEP2-Bw@+A_0TTAam+NVSU~&Di
\card<<<
in the context of generalized derivatives, what do we mean by $\mathcal{D}$?
#
the space of smooth functions with compact support:
$ \mathcal{D} (\Omega) = \{ u \in C^\infty(\Omega) : \textrm{u has compact support in} \Omega \} $
------
We do this because if $ \Omega $ is bounded, every $ u \in \mathcal{D} $ vanishes on the boundary and we can apply integration by parts as often as we need without getting any boundary terms
>>>

ID:KlwUA%617ppLij1yy_TQa05IoE+zW!
\def<<<
generalized derivative
#
for $ u\in \mathcal{D}'(\Omega) $, we define $ g\in \mathcal{D}'(\Omega) $ to be the generalized derivative $ D^\alpha_g u $ of $u$ by

[ $ \langle g,\phi \rangle_{\mathcal{D}'\times \mathcal{D}} = (-1)^{|\alpha|} \langle u, D^\alpha \phi \rangle_{\mathcal{D}'\times \mathcal{D}} \,\, \phi \in \mathcal{D} $ ]
>>>

ID:fmzpX0ji~AgP_vMKsJs&ysj+dbGMRd
\def<<<
The space of locally integrable functions.
#
We call this space [ $L_{loc}^1(\Omega)$ ] and define it as the set of all functions on $ \Omega $ that are [integrable over all compact sets $K$ in $\Omega$]:

$ L^1_{loc}(\Omega) = \{u: u|_K \in L_1(K) \forall \mathrm{compact sets} K \subset \Omega \} $
>>>

ID:rPjDnZ@SIvy*f5GFhX=s7SL9Uecq4X
\def<<<
weak derivative
#
For $ u \in L^1_{loc}(\Omega) $ we call g the [weak derivative $D^\alpha_w u $] if $g\in  L^1_{loc} $ satisfies:
[$ \int_\Omega g \phi dx = (-1)^{|\alpha|} \int_Omega u D^\alpha \phi dx \forall \phi\in \mathcal{D}(\Omega) $]
#
This is the same as saying we call g the weak derivative if it is the generalized derivative and can be identified with an $L^1_{loc}$-function, because we just replace the functional application with an integral
>>>


